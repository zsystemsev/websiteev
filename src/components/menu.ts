import { LitElement, html, css } from 'lit-element';

class Menu extends LitElement {
  static get styles() {
    return css `
      .main {
        background: #232059;
        overflow: hidden;
        display: flex;
        justify-content: space-between;
        align-items: center;
      }

      .menu a {
        float: left;
        color: #fcfcfc;
        text-align: center;
        padding: 20px 16px;
        text-decoration: none;
        font-size: 14px;
        border-style: none;
      }

      .menu {
        display: flex;
        align-items: center;
      }

      .menu a:hover {
        background: #56358B;
        color: #FCFCFC
      }

      .logo {
        color: #fcfcfc;
        font-size: 26px;

        display: flex;
        justify-content: center;
        align-items: center;
      }

      .btn-contact {
        width: auto;
        padding: 9px 14px;
        border: 1px solid #e8ecfa;
        border-radius: 5px;
        cursor: pointer;
        font-family: 'Montserrat', sans-serif;
        font-size: 14px;
        color: #fcfcfc;
        background: transparent;
        transition: all 0.2s linear;
      }

      .btn-contact:hover {
        border-color: #56358B;
        background: #56358B;
        color: #FFF;
      }
    `;
  }

  render() {
    return html `
      <link rel="stylesheet" href="./src/styles/reset.css" >

      <div class="main">
        <div class="logo">
          <h1>ZSYSTEMS</h1>
        </div>

        <div class="menu">
          <a href="#">Home</a>
          <a href="#">Produtos</a>
          <a href="#">Projetos</a>
          <a href="#">Quem Somos</a>
          <a href="#">Serviços</a> 

          <button class="btn-contact">Contato</button>
        </div>
      
      </div>
    `;
  }
}

window.customElements.define('menu-menu', Menu);